# Arduino-401CC

- by allankliu@163.com

This repo was designed for STM32F401CC based black pill board with Arduino IDE.

(20221124T1418XX)

The current project serial protocol simulator to act as a serial dongle for wireless module, since it supports UART and USB ACM
interface, as well as other interfaces connecting to Radio front-end via SPI.

(20221128T1610XX)

Verified on Lenovo Desktop.

## Serial Commander Protocol

Transport Layer (peer to peer) for up/down directions

DLEN | PAYLOAD

### Command Set

Based upon CRUD methods, which was popular in Web and CoAP for IoT.

CREATE | OBJ | PARAMS
UPDATE | OBJ | PARAMS
READ   | OBJ
DELETE | OBJ

RET200/400/500 | PARAMS

OBJ was referred with UUID or 16bit ID
