#include <stdio.h>
#include <string.h>
#include "stm32yyxx_ll_adc.h"

unsigned long tmr = 0;
unsigned long start = 0;
boolean ledOn = true;
char buffer[128];

String inString = "";
bool stringComplete = false;

#define UUID_CHIPID       0xFFFE
#define UUID_VOLTAGE      0x0001
#define UUID_TEMPERATURE  0x0002
#define UUID_RSSI         0x0003

struct PACKET_STRUCT {
  uint8_t   datalen;  // data length for payload
  uint16_t  uuid;     // UUID as the endpoint path
  uint8_t   paralen;  // params length
  uint8_t*  params;   // params
};

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(PC13, OUTPUT);
  // reserve 256 byes for the sing
  inString.reserve(256);

  while(!Serial){
  }
  Serial.println("MISTMESH command handler");
}

void loop() {
  // put your main code here, to run repeatedly:
  //String inString = "";
  /*
  while(Serial.available()>0){
    inString += char(Serial.read());
    ledOn = !ledOn;
  }

  if(inString != ""){
    //Serial.print("Received:");
    Serial.print(inString);
  }
  */
  if(stringComplete){
    //Serial.println(inString);
    Serial.print(inString);
    serialCmdHandler(inString);
    inString = "";
    stringComplete = false;
  }
}

void serialEvent(){
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }  
}

/*
 * Command handler based upon hexdecimal string, which can be converted into 
 * a pure binary protocol command handler.
 * Transport Layer:
 *  DLEN | PAYLOAD
 * PAYLOAD:
 *  CMD | PDLEN
 * CMD:
 * - CREATE (aka POST)
 * - UPDATE (aka PUT)
 * - READ (aka GET)
 * - DELETE (aka DEL)
 */
void serialCmdHandler(String inString){

}

void getByteFromHex(){
}

void getWordFromHex(){
}

void getDWordFromHex(){
}

void getBufferFromHex(){
}

void cmdCreateHandler(){
}

void cmdUpdateHandler(){
}

void cmdReadHandler(){
}

void cmdDeleteHandler(){
}

