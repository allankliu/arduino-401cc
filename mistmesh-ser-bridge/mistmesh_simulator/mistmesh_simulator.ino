/* MistMesh Simulator
 *  MistMesh is a proprietary wireless protocol with IEEE802.15.4 phy
 *  This project works as a dongle interface to a Linux host gateway
 * 
 * Web Referemces:
 *  https://www.programmingelectronics.com/sprintf-arduino/
 *  https://github.com/stm32duino/STM32Examples/blob/main/examples/Peripherals/ADC/Internal_channels/Internal_channels.ino
 * 
 */
#include <stdio.h>
#include <string.h>
#include "stm32yyxx_ll_adc.h"

/* Values available in datasheet */
#define CALX_TEMP 25
#if defined(STM32F1xx)
#define V25       1430
#define AVG_SLOPE 4300
#define VREFINT   1200
#elif defined(STM32F2xx) || defined(STM32F4xx)
#define V25       760
#define AVG_SLOPE 2500
#define VREFINT   1210
#endif

/* Analog read resolution */
#define LL_ADC_RESOLUTION LL_ADC_RESOLUTION_12B
#define ADC_RANGE 4096
 
#define INTERVAL 500;

unsigned long tmr = 0;
unsigned long start = 0;
boolean ledOn = true;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(PC13, OUTPUT);
  //digitalWrite(PC13, ledOn);
  
  while(!Serial){
    ;
  }
  Serial.println("MISTMESH Simmulator");
  analogReadResolution(12);
}

static int32_t readVref()
{
#ifdef __LL_ADC_CALC_VREFANALOG_VOLTAGE
#ifdef STM32U5xx
  return (__LL_ADC_CALC_VREFANALOG_VOLTAGE(ADC1, analogRead(AVREF), LL_ADC_RESOLUTION));
#else
  return (__LL_ADC_CALC_VREFANALOG_VOLTAGE(analogRead(AVREF), LL_ADC_RESOLUTION));
#endif
#else
  return (VREFINT * ADC_RANGE / analogRead(AVREF)); // ADC sample to mV
#endif
}

#ifdef ATEMP
static int32_t readTempSensor(int32_t VRef)
{
#ifdef __LL_ADC_CALC_TEMPERATURE
#ifdef STM32U5xx
  return (__LL_ADC_CALC_TEMPERATURE(ADC1, VRef, analogRead(ATEMP), LL_ADC_RESOLUTION));
#else
  return (__LL_ADC_CALC_TEMPERATURE(VRef, analogRead(ATEMP), LL_ADC_RESOLUTION));
#endif
#elif defined(__LL_ADC_CALC_TEMPERATURE_TYP_PARAMS)
  return (__LL_ADC_CALC_TEMPERATURE_TYP_PARAMS(AVG_SLOPE, V25, CALX_TEMP, VRef, analogRead(ATEMP), LL_ADC_RESOLUTION));
#else
  return 0;
#endif
}
#endif

static int32_t readVoltage(int32_t VRef, uint32_t pin)
{
#ifdef STM32U5xx
  return (__LL_ADC_CALC_DATA_TO_VOLTAGE(ADC1, VRef, analogRead(pin), LL_ADC_RESOLUTION));
#else
  return (__LL_ADC_CALC_DATA_TO_VOLTAGE(VRef, analogRead(pin), LL_ADC_RESOLUTION));
#endif
}

void loop() {
  char buffer[128];
  // put your main code here, to run repeatedly:
  unsigned long _now = millis();
  int value = 100;
  int32_t VRef = readVref();
    
  if ((_now - start) > 500){
    digitalWrite(PC13, ledOn);
    ledOn = !ledOn;
    start = _now;
    sprintf(buffer, "{\"guid\":\"DAD1BEEFCAFE0001\",\"dt_type\":\"temperature\",\"value\":%i,\"led\":%i}", readTempSensor(VRef), (int8_t)ledOn);
    Serial.println(buffer);
  }
}
